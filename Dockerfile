FROM docker.algotrader.ch/algotrader:latest

ENV STRATEGY_NAME=FXICHIMOKU

WORKDIR /usr/local/strategy
ADD target/*.jar lib

ENTRYPOINT ["/usr/local/algotrader/bin/docker-strategy-run.sh"]
CMD ["-e"]
