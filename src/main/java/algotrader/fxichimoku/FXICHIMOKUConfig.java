package algotrader.fxichimoku;

import ch.algotrader.config.ConfigName;

/**
 * The strategy configuration class
 */
public class FXDELTAConfig {

    private long accountId;
    private long securityId;
    private long orderQuantity;

    public FXDELTAConfig(
        @ConfigName("accountId") final long accountId,
        @ConfigName("securityId") final long securityId,
        @ConfigName("orderQuantity") final long orderQuantity) {

        this.accountId = accountId;
        this.securityId = securityId;
        this.orderQuantity = orderQuantity;
    }

    public long getAccountId() {
        return accountId;
    }

    public long getSecurityId() {
        return securityId;
    }

    public long getOrderQuantity() {
        return orderQuantity;
    }

    @Override
    public String toString() {
        return "FXDELTAConfig {" +
            "accountId=" + accountId +
            "securityId=" + securityId +
            "orderQuantity=" + orderQuantity +
        "}";
    }
}
