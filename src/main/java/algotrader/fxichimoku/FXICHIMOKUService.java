package algotrader.fxichimoku;

import static ch.algotrader.util.TA4JUtil.toTick;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;

import org.ta4j.core.BaseTimeSeries;

import algotrader.fxichimoku.Signal;
import ch.algotrader.service.ConfigAwareStrategyService;

import ch.algotrader.entity.marketData.BarVO;
import ch.algotrader.entity.trade.MarketOrderVO;
import ch.algotrader.entity.trade.MarketOrderVOBuilder;
import ch.algotrader.entity.trade.OrderStatusVO;
import ch.algotrader.enumeration.AdapterType;
import ch.algotrader.enumeration.Side;
import ch.algotrader.enumeration.Status;
import ch.algotrader.vo.LifecycleEventVO;

/**
 * The main service class of the FXICHIMOKU strategy
 */
public class FXICHIMOKUService extends ConfigAwareStrategyService<FXICHIMOKUConfig> {

	private final long accountId = 100;
	private final int securityId = 25;
	private double pipsize = 0.0001;
	private boolean onMarket = false, orderPending = false;
	private BigDecimal orderQuantity, equity = new BigDecimal("100000");
	private final String defaultAdapterType = "SIM";
	private ArrayList<Signal> signals;
	private Signal currentPosition;
	private BarVO currentBar;

	private BaseTimeSeries series;
	// add indicators and params HERE

	@Override
	public void onStart(final LifecycleEventVO event) {
		getSubscriptionService().subscribeMarketDataEvent(getStrategyName(), this.securityId, defaultAdapterType);
	}

	@Override
	public void onInit(final LifecycleEventVO event) {
		this.signals = new ArrayList<Signal>();
		this.series = new BaseTimeSeries();
		// initialize indicators and params here
	}

	private void checkSignal(final int i) {
		// check for valid signals here
		boolean isSignal = false;

		if (isSignal) {
			// Signal curSignal = new Signal();
			// this.signals.add(curSignal);
			// generate and add a signal and add to signals `ArrayList`
			// this.signals.add(new Signal(/* signal constructor of choice */));
		}
	}

	private void checkEntry() {
		for (Signal s : this.signals) {
			// check your entry conditions to enter a position
			if (this.currentBar.getLow().doubleValue() <= s.entry
					&& s.entry <= this.currentBar.getHigh().doubleValue()) {
				this.currentPosition = s;
				sendEntryOrder(s);
				s.tradePlaced(); // signal that a trade has been placed (will
									// remove it after this candle)
				break;
			}
		}
	}

	private void checkExit() {
		// check for SL/TP if in a position
		boolean isSL = this.currentBar.getLow().doubleValue() <= this.currentPosition.sl
				&& this.currentPosition.sl <= this.currentBar.getHigh().doubleValue(),
				isTP = this.currentBar.getLow().doubleValue() <= this.currentPosition.tp
						&& this.currentPosition.tp <= this.currentBar.getHigh().doubleValue();

		if (isSL || isTP) {
			sendExitOrder((isTP) ? this.currentPosition.tp : this.currentPosition.sl,
					this.currentPosition.getPosition());
		}
	}

	/**
	 * @brief Sends the order predicated by `Signal` s to the broker
	 */
	private void sendEntryOrder(Signal s) {

		this.equity = getPortfolioService().getCashBalance(getStrategyName());
		this.orderQuantity = ((equity.multiply(new BigDecimal(.01)))
				.divide(new BigDecimal(s.stopLoss * this.pipsize * 100000), 5, RoundingMode.HALF_UP))
						.multiply(new BigDecimal(100000));

		MarketOrderVO order = MarketOrderVOBuilder.create().setStrategyId(getStrategy().getId())
				.setAccountId(this.accountId).setSecurityId(this.securityId).setQuantity(this.orderQuantity)
				.setSide(s.getPosition().equals("LONG") ? Side.BUY : Side.SELL_SHORT).build();

		this.orderPending = true;
		getOrderService().sendOrder(order);
	}

	private void sendExitOrder(double price, String side) {
		MarketOrderVO order = MarketOrderVOBuilder.create().setStrategyId(getStrategy().getId())
				.setAccountId(this.accountId).setSecurityId(this.securityId).setQuantity(this.orderQuantity)
				.setSide(side.equals("LONG") ? Side.SELL : Side.BUY).build();

		this.orderPending = true;
		getOrderService().sendOrder(order);
	}

	@Override
	public void onOrderStatus(OrderStatusVO orderStatus) {
		if (orderStatus.getStatus().equals(Status.EXECUTED)) {
			if (!this.onMarket) { // entry
				this.onMarket = true;
				this.orderPending = false;
			} else { // exit
				this.orderPending = false;
				this.onMarket = false;
			}
		}
	}

	@Override
	public void onBar(BarVO bar) {
		// *DO NOT* change this function
		this.currentBar = bar;
		this.series.addBar(toTick(this.currentBar));
		final int i = this.series.getEndIndex();


		if (!this.onMarket && !this.orderPending)
		checkEntry();
		if (!this.orderPending && this.onMarket)
		checkExit();

		// remove all signals timed-out on the last candle
		ArrayList<Signal> invalidSignals = new ArrayList<Signal>();
		for (Signal s : this.signals)
			if (!s.isValid)
				invalidSignals.add(s);
		this.signals.removeAll(invalidSignals);

		// update all signal timeouts
		for (Signal s : this.signals)
		s.updateTimeout();

		checkSignal(i);
	}
}
